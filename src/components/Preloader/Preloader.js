import React from 'react';
import svg from "./loader.svg";
import "./Preloader.scss"

const Preloader = () => {
  return (<div className="preloader">
      <img src={svg} alt="Loader" />
  </div>)
}

export default Preloader