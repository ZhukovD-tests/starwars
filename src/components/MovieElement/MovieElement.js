import React from 'react';
import "./../MovieElement/MovieElement.scss";

class MovieElement extends React.Component {
	// constructor(props) {
	//   super(props)
	// }
  
	render() {
	  const { title, director, producer, release_date } = this.props
      
      return <div className="movie-elem">
          <img src={"https://loremflickr.com/440/220/starwars?lock=" + release_date} alt="starwars movie"/>
          <h3>{title}</h3>
          <div className="movie-elem-date">{release_date}</div>
          <div className="movie-elem-text">
            <div className="movie-elem-creator director">
                <div className="role">Director: </div>
                <div className="names">{director}</div>
            </div>
            <div className="movie-elem-creator producer">
                <div className="role">Producer: </div>
                <div className="names">{producer}</div>
            </div>
          </div>
      </div>
	}
  }
export default MovieElement