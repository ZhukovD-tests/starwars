import React from 'react';
import Preloader from "./../Preloader/Preloader";
import ContentBlock from "./../ContentBlock/ContentBlock";
import Comment from "./../Comment/Comment";
import "./Movie.scss";
import { Link } from 'react-router-dom'

class Movie extends React.Component {

  constructor(props) {
	  super(props)
    this.state = {
      lazyLoadData: [],
      lazyLoadList: {
        characters: false, 
        planets: false, 
        species: false, 
        starships: false, 
        vehicles: false
      },
      lazyLoadImages: {
        characters: 0, 
        planets: 0, 
        species: 0, 
        starships: 0, 
        vehicles: 0
      },
      error: null,
      isLoaded: false,
      data: [],
    }
  }

  loadImages = (propName, data) => {
    let imagesLoaded = 0;
    data.map((elem, i) => {
      let img = new Image();
				
      img.onload = (e) => {
        imagesLoaded++;
        if(imagesLoaded === data.length) {
          this.setState({
            lazyLoadList: {
              ...this.state.lazyLoadList,
              [propName]: true
            }
          }, () => {
            if(this.requestsLimit) {
              this.startLazyLoading();
              --this.requestsLimit;
            }
          }
          )
        }
      }
      
      img.src = "https://loremflickr.com/220/150/starwars," + propName + "?lock=" + i;
    })
     
  }

  requestsLimit = 10; //to prevent IP ban on API
  loadBlock = async (propName) => {
    const urls = this.state.data[propName];
    if(!urls) return;

    await Promise.all(urls.map(url => fetch(url)))
    .then(rawRes => {
      Promise.all(rawRes.map(res => res.json()))
        .then(res => {
          this.setState({
            lazyLoadData: [
              ...this.state.lazyLoadData,
              {
                name: propName,
                data: res
              }
            ],
            // lazyLoadList: {
            //   ...this.state.lazyLoadList,
            //   [propName]: true
            // }
          }, () => {
            this.loadImages(propName, res);
          })
        })
    })
  }

  startLazyLoading = async () => {
    let nextPropName = null;
    for(let propName in this.state.lazyLoadList) {
      if(!this.state.lazyLoadList[propName]) {
        nextPropName = propName;
        break;
      }
    }

    if(nextPropName) await this.loadBlock(nextPropName);
    
  }

  componentDidMount() {
    const response = async () => {
      const movieId = this.props.match.params.movieId;
      await fetch(`https://swapi.dev/api/films/${movieId}`)
      .then((res) => res.json())
      .then(
        (result) => {
          this.setState({
            isLoaded: true,
            data: result,
          })

          if(result) this.startLazyLoading();
        },
        (error) => {
          this.setState({
            isLoaded: true,
            error,
          })
        }
      )
    }
    response();
  };
  
  backBtn = (
      <Link to={"/"} className="movie-btn-back">
        &lt;BACK
      </Link>
  )

  render() {
    const { error, isLoaded, data, lazyLoadData, lazyLoadList } = this.state;
    // console.log(data);
    if (error) {
      return (<div className="movie">
          {this.backBtn}
          Ошибка: {error.message}
        </div>)
    } else if (!isLoaded) {
      return <Preloader/>
    } else {
      return (<div className="movie">
          {this.backBtn}
          <img src={"https://loremflickr.com/440/220/starwars?lock=" + data.release_date} alt="starwars movie" />
          <h1>{data.title}</h1>
          <h2 className="release-date">{data.release_date}</h2>
          <p className="description">{data.opening_crawl}</p>
          <div className="movie-creators">
            <div className="movie-creators director">
                <div className="role">Director:&nbsp;</div>
                <div className="names">{data.director}</div>
            </div>
            <div className="movie-creators producer">
                <div className="role">Producer:&nbsp;</div>
                <div className="names">{data.producer}</div>
            </div>
          </div>

          {Object.keys(lazyLoadList).map((propName, i) => (
            <ContentBlock key={propName} name={propName} data={lazyLoadData[i]} />
          ))}
          <Comment />
        </div>)
    }
  }
}

export default Movie