import './App.scss';
import MovieList from "./MovieList/MovieList";
import Movie from "./Movie/Movie";

import { BrowserRouter as Router, Route } from 'react-router-dom'

function App() {
  return (
    <div className="App">
      <Router>
        <Route path="/:movieId" component={Movie} />
        <Route exact path="/" component={MovieList} />
      </Router>
    </div>
  );
}

export default App;
