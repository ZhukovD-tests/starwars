import React from 'react';
import "./Comment.scss"

class Comment extends React.Component {
	constructor(props) {
    super()
    this.state = {
      formState: null,
      formText: null,
    }
    this.inputSubmit = React.createRef();
  }
  
  submitForm = async (e) => {
    e.preventDefault();
    const 
        eMailMask = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
        formData = new FormData(e.target),
        email = formData.get("mail"),
        username = formData.get("username"),
        text = formData.get("text"),
        formState = eMailMask.test(String(email).toLowerCase()),
        submitBtn = this.inputSubmit.current,
        formText = `${username} (${email}),\nyour comment successfully sent:\n${text}`;

    submitBtn.innerText = "Sending...";
    submitBtn.disabled = true;
    await new Promise((resolve) => { setTimeout(resolve, 1000) })

    this.setState({
      formState,
      formText: (formState)?formText:"Something wrong. Try again" 
    }, () => {
      if(formState) e.target.reset();
      submitBtn.innerText = "Send";
      submitBtn.disabled = false;
      const formResetDelay = (formState)?5000:1000;
      setTimeout(() => {
        this.setState({
          formState: null,
          formText: null 
        })   
      }, formResetDelay)
    })
  }

  formResultBlock = () => {
    const {formState} = this.state;
    if(formState === null) {
      return "form-result null"
    } else if(formState === true) {
      return "form-result success"
    } else if(formState === false) {
      return "form-result error"
    }
  }

	render() {
    return (
      <div className="form-wrapper">
        <h2>Send your opinion:</h2>
        <form onSubmit={this.submitForm}>
          <label>
            Username:
            <input name="username" required />
          </label>
          <label>
            E-Mail:
            <input type="mail" name="mail" required />
          </label>
          <label>
            Text:
            <textarea name="text" required/>
          </label>
          <button type="submit" ref={this.inputSubmit}>Send</button>
          
          <div className={this.formResultBlock()}>
            {this.state.formText}
          </div>
        </form>
      </div>
    )
	}
}
export default Comment