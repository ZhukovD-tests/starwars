import React from 'react';
import MovieElement from "./../MovieElement/MovieElement";
import Preloader from "./../Preloader/Preloader";
import "./MovieList.scss";

import { Link } from 'react-router-dom'

class MovieList extends React.Component {
	constructor(props) {
	  super(props)
	  this.state = {
		error: null,
		isLoaded: false,
		items: [],
		imgsLoaded: 0
	  }
	}
  
	componentDidMount() {
	  fetch('https://swapi.dev/api/films/')
		.then((res) => res.json())
		.then(
		  (result) => {
			this.setState({
			  items: result.results,
			})
			return result;
		  },
		  (error) => {
			this.setState({
			  isLoaded: true,
			  error,
			})
			return error;
		  }
		)
		.then((res) => {
			res.results.map(movie => {
				let img = new Image();
				
				img.onload = (e) => {
					this.setState({
						imgsLoaded: this.state.imgsLoaded + 1
					}, () => {
						if(this.state.imgsLoaded === res.results.length) {
							this.setState({
								isLoaded: true
							})
						}
					})
				}
				
				img.src = "https://loremflickr.com/440/220/starwars?lock=" + movie.release_date;
			})
		})
	}
  
	render() {
	  const { error, isLoaded, items } = this.state
	  if (error) {
		return <div>Ошибка: {error.message}</div>
	  } else if (!isLoaded) {
		return <Preloader/>
	  } else {
		return (
			<div className="movie-list">
				{items.map((item, i) => (
					<Link key={i} to={"/" + (i+1)} className="movie-elem-wrapper">
						<MovieElement key={item.episode_id} {...item} />
					</Link>
				))}
			</div>
		)
	  }
	}
  }
export default MovieList