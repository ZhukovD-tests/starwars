import React from 'react';
import "./ContentBlock.scss";
import Preloader from "./../Preloader/Preloader";

class ContentBlock extends React.Component {
	constructor(props) {
	  super()
    this.scrollDiv = React.createRef();
  }
  
  formatData(rawData) {
    let data = [];
    rawData.map(rawElem => {
      let elem = {};
      for(let prop in rawElem) {
        const isPropValid = typeof rawElem[prop] === "string" && //not an array
                            rawElem[prop].indexOf("http") === -1 && //not a url
                            prop !== "created" && prop !== "edited"; //not a technical info
        if(isPropValid) {
          elem[prop.replace(/_/gi, " ")] = rawElem[prop];
        }
      }
      data.push(elem);
    })
    return data;
  }

  handleScroll = (e) => {
    e.preventDefault();
    const step = 350;
    this.scrollDiv.current.scrollLeft += (e.deltaY > 0)?step:-step;
  }
  
  componentDidMount() {
    if(this.scrollDiv.current)
    this.scrollDiv.current.addEventListener('wheel', this.handleScroll, { passive: false });
  }

	render() {
    const {name} = this.props;
    if(this.props.data && this.props.data.name) {
      const data = this.formatData(this.props.data.data);
      // this.setScrollHandler();
      return (
        <div className="content-block">
          <h2 className="content-block-name">{name}</h2>
          <div className="content-block-list" ref={this.scrollDiv}>
            {data.map((prop,i) => (
              <div key= {name + i} className="content-block-list element">
                <img 
                  className="prop-img" 
                  src={"https://loremflickr.com/220/150/starwars," + name + "?lock=" + i} 
                  alt={"starwars " + name}
                />
                <ul key={name + i + "_ul"}>
                  {Object.keys(prop).map((propName, j) => (
                    <li key={name + propName + j}>
                      <span className="prop-name">{propName}: </span>
                      <span className="prop-value">{prop[propName]}</span>
                    </li>
                  ))}
                </ul>
              </div>
            ))}
          </div>
        </div>
      )
    } else {
      return (
        <div className="content-block">
          <h2 className="content-block-name">{name}</h2>
          <div className="content-block-list" ref={this.scrollDiv}>
            <Preloader />
          </div>
        </div>
      )
    }
	}
}
export default ContentBlock